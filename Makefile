CC=g++
CFLAGS=-c -Wall
LDFLAGS=`pkg-config --libs opencv`
SOURCES=opencv_version.cpp
OBJECTS=$(SOURCES:.cpp=.o)
EXECUTABLE=posterize

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(OBJECTS) 
	$(CC) $(LDFLAGS) $(OBJECTS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $< -o $@

clean:
	rm *.o posterize
